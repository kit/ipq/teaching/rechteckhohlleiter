import dash
import dash_bootstrap_components as dbc
from library.setup import layout, callbacks

external_stylesheets = [dbc.themes.BOOTSTRAP]
external_scripts=[]

meta_tags=[
    # A tag that tells the browser not to scale
    # desktop widths to fit mobile screens.
    # Sets the width of the viewport (browser)
    # to the width of the device, and the zoom level
    # (initial scale) to 1.
    #
    # Necessary for "true" mobile support.
    {
      'name': 'viewport',
      'content': 'width=device-width, initial-scale=1.0',
      'lang': 'de' # set the language for hyphenation https://medium.com/clear-left-thinking/all-you-need-to-know-about-hyphenation-in-css-2baee2d89179
    }
]

# Note that the requests_pathname_prefix will result in nothing but loading screen showing up without the apache integration
app = dash.Dash(__name__, external_stylesheets=external_stylesheets, external_scripts=external_scripts, meta_tags=meta_tags, requests_pathname_prefix='/rechteckhohlleiter/')

app.layout = layout(debug=False)
callbacks(app)


if __name__ == '__main__':
    app.run_server(debug=False)
