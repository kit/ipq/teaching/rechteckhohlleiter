from numpy import *



# Datenpunkte & Zeitachse
Mx = 61 # Anzahl Auswertungspunkte in x-Richtung
My = 61 # Anzahl Auswertungspunkte in y-Richtung
Mz = 61 # Anzahl Auswertungspunkte in z-Richtung


# Materialparameter
µ0 = 1.2566370621219e-6 # Magnetische Feldkonstante µ0
ϵ0 = 8.854187812813e-12 # Elektrische Feldkonstante eps0

class wgsetup:
    def __init__(self, Mx, My, Mz, m, n, a, b, f, pol):
        # Gewünschte Mode
        self.m = m
        self.n = n

        # Höhe des Wellenleiters [in m]
        self.a = a*1e-3 # 50e-3
        # Breite des Wellenleiters [in m]
        self.b = b*1e-3 # 50e-3

        # Relative Permittivität des Füllmaterials 
        ϵr = 1
        # Relative Permeabilität des Füllmaterials 
        µr = 1
        # Permittivität
        self.ϵ = ϵ0*ϵr
        # Permeabilität 
        self.µ = µ0*µr
        # Lichtgeschwindigkeit
        self.c = 1/sqrt(self.µ*self.ϵ)

        # Frequenz der gewählten Mode
        self.f = f*1e9
        # Wellenlänge der gewählten Mode
        self.λ = self.c/self.f


        # Wellenzahl in x-Richtung
        self.kx = pi*m/self.a
        # Wellenzahl in y-Richtung
        self.ky = pi*n/self.b
        # Cut-off-Frequenz der gewählten Mode
        self.fc = self.c/(2*pi)*sqrt(self.kx**2+self.ky**2)
        # Operationskreisfrequenz
        self.ω = 2*pi*self.f
        # Betrag des Wellenvektors (Wellenzahl)
        self.k = self.ω/self.c
        # Wellenzahl in z-Richtung
        self.kz2 = self.k**2-self.kx**2-self.ky**2
        if self.kz2 < 0:
            self.kz = -1j*sqrt(-self.kz2)
        else:
            self.kz = sqrt(self.kz2)

        # Ausdehnung des Plots in z-Richtung
        self.L = 2*self.λ

        # Zeitelement für statischen Plot
        self.tp = 2*pi/self.ω/4

        self.Mx = Mx
        self.My = My
        self.Mz = Mz
        
        # Erstellung des Grids
        self.x = linspace(0, self.a, num=Mx, endpoint=True)
        self.y = linspace(0, self.b, num=My, endpoint=True)
        self.z = linspace(0, self.L, num=Mz, endpoint=True)

        # Wellenleitergeometrie
        self.xywg = dict(x=array([0,self.a,self.a,0,0,]), y=array([0,0,self.b,self.b,0,]))
        self.xzwg = dict(x=array([0,self.a,self.a,0,0,]), y=array([0,0,self.L,self.L,0,]))
        self.yzwg = dict(x=array([0,self.b,self.b,0,0,]), y=array([0,0,self.L,self.L,0,]))

        self.wg3d = dict(
            x = array([0,self.a,self.a,0,0,nan, # bottom
                0,self.a,self.a,0,0,nan, # top
                0,0,nan, # front left
                self.a,self.a,nan, # front right
                self.a,self.a,nan, # back right
                0,0,nan # back left
                ]),
            y = array([0,0,0,0,0,nan, # bottom
                self.b,self.b,self.b,self.b,self.b,nan, # top
                0,self.b,nan, # front left
                0,self.b,nan, # front right
                0,self.b,nan, # back right
                0,self.b,nan # back left
                ]),
            z = array([0,0,self.L,self.L,0,nan, # bottom
                0,0,self.L,self.L,0,nan, # top
                0,0,nan, # front left
                0,0,nan, # front right
                self.L,self.L,nan, # back right
                self.L,self.L,nan # back left
                ]),
        )

            # can current mode propagate?
        if pol == "H": # TE
            if (self.kz2 >= 0) and (self.m or self.n):
                self.propagates = True
            else:
                self.propagates = False
        else: # TM
            if (self.kz2 >= 0) and (self.m and self.n):
                self.propagates = True
            else:
                self.propagates = False