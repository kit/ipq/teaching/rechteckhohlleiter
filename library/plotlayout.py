#--------------------------------------------------------------------------
# LAYOUT DEFINITION
#--------------------------------------------------------------------------
layout1d = dict(
    xaxis = dict(
        showline = False,
        zeroline = True,
        zerolinecolor = '#444444',
        ticks = 'outside',
    ),
    yaxis = dict(
        showline = False,
        zeroline = True,
        zerolinecolor = '#444444',
        ticks = 'outside',
        constrain = 'domain',
    ),
    font = dict(
        size = 16,
        color = '#444444'
    ),
    margin = dict(l=0,r=0,t=20,b=30),
    showlegend = True,
    template = "plotly_white",
)

layout2d = dict(
    xaxis = dict(
        showline = False,
        zeroline = False,
        zerolinecolor = '#444444',
        ticks = 'outside',
        constrain = 'domain',
    ),
    yaxis = dict(
        showline = False,
        zeroline = False,
        zerolinecolor = '#444444',
        ticks = 'outside',
        scaleanchor = 'x',
        scaleratio = 1,
        constrain = 'domain',
    ),
    font = dict(
        size = 16,
        color = '#444444'
    ),
    margin = dict(l=0,r=0,t=20,b=30),
    template = "plotly_white",
)

layout3d = dict(
    scene = dict(
        xaxis = dict(
            showline = False,
            zeroline = False,
            zerolinecolor = '#444444',
            ticks = 'outside',
            autorange = False,
            title = 'z',
        ),
        yaxis = dict(
            showline = False,
            zeroline = False,
            zerolinecolor = '#444444',
            autorange = False,
            ticks = 'outside',
            title = 'y',
        ),
        zaxis = dict(
            showline = False,
            zeroline = False,
            zerolinecolor = '#444444',
            autorange = False,
            ticks = 'outside',
            title = 'x',
        ),
        aspectmode = 'data',
        camera = dict(
            eye = dict(
                x = 2,
                y = 2,
                z = 2
            )
        ),
    ),
    margin = dict(l=0,r=0,t=20,b=30),
    font = dict(
        size = 16,
        color = '#444444'
    ),
    template = "plotly_white",
)