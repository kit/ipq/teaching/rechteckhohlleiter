from numpy import *

class modefield:
    def __init__(self, F, Flim):
        self.F = F
        self.Flim = Flim

def generate_field(wg, pol, fcomp, t, plane, idx, fun):
    
    if plane == "xy":
        x = wg.x
        y = wg.y
        z = array([wg.z[idx]])
    elif plane == "xz":
        x = wg.x
        y = array([wg.y[idx]])
        z = wg.z
    elif plane == "yz":
        x = array([wg.x[idx]])
        y = wg.y
        z = wg.z
    else:
        x = wg.x
        y = wg.y
        z = wg.z

    kx = wg.kx
    ky = wg.ky
    kz = wg.kz
    ω = wg.ω
    ϵ = wg.ϵ
    µ = wg.µ

    dims = (len(x),len(y),len(z))

    F = 0
    Flim = 0
    
    # Berechnung der Feldgrößen
    if pol == "E": # E-Welle / TM
        if fcomp == "Ex":
            F = einsum('i,j,k', kz/(kx**2+ky**2)*kx * cos(kx*x), sin(ky*y), -1j*exp(1j*(ω*t - kz*z)))
            Flim = kz/(kx**2+ky**2)*kx
        elif fcomp == "Ey":
            F = einsum('i,j,k', kz/(kx**2+ky**2)*ky * sin(kx*x), cos(ky*y), -1j*exp(1j*(ω*t - kz*z)))
            Flim = kz/(kx**2+ky**2)*ky
        elif fcomp == "Ez":
            F = einsum('i,j,k', sin(kx*x), sin(ky*y), exp(1j*(ω*t - kz*z)))
            Flim = 1

        elif fcomp == "Hx":
            F = einsum('i,j,k', ω*ϵ/(kx**2+ky**2)*ky * sin(kx*x), cos(ky*y), 1j*exp(1j*(ω*t - kz*z)))
            Flim = ω*ϵ/(kx**2+ky**2)*ky
        elif fcomp == "Hy":
            F = einsum('i,j,k', ω*ϵ/(kx**2+ky**2)*kx * cos(kx*x), sin(ky*y), -1j*exp(1j*(ω*t - kz*z)))
            Flim = ω*ϵ/(kx**2+ky**2)*kx
        elif fcomp == "Hz":
            F = zeros(dims)
            Flim = 0

        elif fcomp == "Sx":
            F = einsum('i,j,k', -1/2*ω*conj(ϵ/(kx**2+ky**2)) * kx * sin(kx*x)*cos(kx*x), sin(ky*y)**2, 1j*exp(-1j*(kz - conj(kz))*z))
            Flim = 1/2*fun(1j/2*ω*conj(ϵ/(kx**2+ky**2)) * kx) # the sin / cos products do not exceed 1/2
        elif fcomp == "Sy":
            F = einsum('i,j,k', -1/2*ω*conj(ϵ/(kx**2+ky**2)) * ky * sin(kx*x)**2, sin(ky*y)*cos(ky*y), 1j*exp(-1j*(kz - conj(kz))*z))
            Flim = 1/2*fun(1j/2*ω*conj(ϵ/(kx**2+ky**2)) * ky) # the sin / cos products do not exceed 1/2
        elif fcomp == "Sz":
            F = einsum('ij..., k', 
                einsum('i,j', kx**2 * cos(kx*x)**2, sin(ky*y)**2) + 
                einsum('i,j', ky**2 * sin(kx*x)**2, cos(ky*y)**2), 
                1/2*kz*ω*conj(ϵ)/abs(kx**2+ky**2)**2 * exp(-1j*(kz - conj(kz))*z))
            Flim = fun(1/2*kz*ω*conj(ϵ)/abs(kx**2+ky**2)**2 * maximum(kx,ky)**2)


    elif pol == "H": # H-Welle / TE
        if fcomp == "Ex":
            F = einsum('i,j,k', ω*µ/(kx**2+ky**2)*ky * cos(kx*x), sin(ky*y), 1j*exp(1j*(ω*t - kz*z)))
            Flim = ω*µ/(kx**2+ky**2)*ky
        elif fcomp == "Ey":
            F = einsum('i,j,k', ω*µ/(kx**2+ky**2)*kx * sin(kx*x), cos(ky*y), -1j*exp(1j*(ω*t - kz*z)))
            Flim = ω*µ/(kx**2+ky**2)*kx
        elif fcomp == "Ez":
            F = zeros(dims)
            Flim = 0

        elif fcomp == "Hx":
            F = einsum('i,j,k', kz/(kx**2+ky**2)*kx * sin(kx*x), cos(ky*y), 1j*exp(1j*(ω*t - kz*z)))
            Flim = kz/(kx**2+ky**2)*kx
        elif fcomp == "Hy":
            F = einsum('i,j,k', kz/(kx**2+ky**2)*ky * cos(kx*x), sin(ky*y), 1j*exp(1j*(ω*t - kz*z)))
            Flim = kz/(kx**2+ky**2)*ky
        elif fcomp == "Hz":
            F = einsum('i,j,k', cos(kx*x), cos(ky*y), exp(1j*(ω*t - kz*z)))
            Flim = 1

        elif fcomp == "Sx":
            F = einsum('i,j,k', -1/2*ω*µ/(kx**2+ky**2) * kx * sin(kx*x)*cos(kx*x), cos(ky*y)**2, 1j*exp(-1j*(kz - conj(kz))*z))
            Flim = 1/2*fun(1j/2*ω*µ/(kx**2+ky**2) * kx) # the sin / cos products do not exceed 1/2
        elif fcomp == "Sy":
            F = einsum('i,j,k', -1/2*ω*µ/(kx**2+ky**2) * ky * cos(kx*x)**2, sin(ky*y)*cos(ky*y), 1j*exp(-1j*(kz - conj(kz))*z))
            Flim = 1/2*fun(1j/2*ω*µ/(kx**2+ky**2) * ky) # the sin / cos products do not exceed 1/2
        elif fcomp == "Sz":
            F = einsum('ij..., k', 
                einsum('i,j', kx**2 * sin(kx*x)**2, cos(ky*y)**2) + 
                einsum('i,j', ky**2 * cos(kx*x)**2, sin(ky*y)**2), 
                1/2*kz*ω*µ/abs(kx**2+ky**2)**2 * exp(-1j*(kz - conj(kz))*z))
            Flim = fun(1/2*kz*ω*µ/abs(kx**2+ky**2)**2 * maximum(kx,ky)**2)

    F = fun(F)
    if not isreal(Flim):
        Flim = fun(Flim)
        
    return modefield(F, Flim)