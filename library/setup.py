from library.wgsetup import *
from library.modefield import *
from library.plotlayout import *

import numpy

import dash
from dash import dcc, html
import dash_bootstrap_components as dbc

from dash.dependencies import Input, Output, State
from dash.exceptions import PreventUpdate

import plotly.graph_objs as go



#--------------------------------------------------------------------------
# DATA FOR DASH APP
#--------------------------------------------------------------------------
polarizations = ["H","E"] # H-Welle / E-Welle
fcomps = ["Ex", "Ey", "Ez", "Hx", "Hy", "Hz", "Sx", "Sy", "Sz"]
views = ["xy", "xz", "yz", "3D"]
mathfun_labels = ["Realteil", "Imaginärteil", "Betrag", "Phase"]
mathfun = ["real", "imag", "abs", "angle"]
propagation_axis = ["f", "a", "b"]
propagation_axis_label = ["Frequenz", "Breite", "Höhe"]
wavelength_axis = ["f", "a", "b"]
wavelength_axis_label = ["Frequenz", "Breite", "Höhe"]




def layout(debug=False):

    # see https://dash-bootstrap-components.opensource.faculty.ai/docs/components/form/ for general information and https://getbootstrap.com/docs/4.3/utilities/spacing/ for className choices
    return dbc.Container(
        [
            dbc.Col(
                [
                    dbc.Card(
                        dbc.CardBody(
                            [
                                html.H1("Moden im Recht\xadeck\xadhohl\xadleiter"), # add soft hyphens via \xad
                                html.Hr(),
                                dcc.Markdown(r"Wir betrachten einen Rechteckhohlleiter der Breite $a$ und Höhe $b$ mit ideal leitenden Wänden. Darin breitet sich eine elektromagnetische Welle mit der Frequenz $f$ aus. $\lambda_0$ bezeichnet die zugehörige Vakuumwellenlänge. Hinweis: Auf schmalen Bildschirmen ist die 3D-Ansicht unter Umständen etwas zu weit hineingezoomt.", mathjax=True),
                            ],
                        ),
                        style={'border': 'none'},
                    )
                ],
            ),

            dbc.Col(
                dbc.Tabs(
                    [
                        dbc.Tab(
                            #--------------------------------------------------------------------------
                            # FIELD PLOT TAB
                            #--------------------------------------------------------------------------
                            dbc.CardBody(
                                [
                                    #--------------------------------------------------------------------------
                                    # FIELD PLOT
                                    #--------------------------------------------------------------------------
                                    dbc.Row(
                                        [
                                            dbc.Col(
                                                html.H5("Plot Feldverteilung von "),
                                                width="auto",
                                            ),
                                            dbc.Col(
                                                #--------------------------------------------------------------------------
                                                # COMPONENTS
                                                #--------------------------------------------------------------------------
                                                dbc.DropdownMenu(
                                                    label=fcomps[0],
                                                    children=[dbc.DropdownMenuItem(i, id=i+"_dropitem") for i in fcomps],
                                                    id='fcomp-dropdown',
                                                    # toggle_style={'padding': '0', 'border': '0', 'fontSize': '20px', 'fontWeight': '500', 'lineHeight': '24px', 'marginBottom': '8px', 'marginLeft': '0.25em'},
                                                ),
                                                width="auto",
                                            )
                                        ],
                                    ),
                                    html.Hr(),
                                    dcc.Graph(id='field-graph', config=dict(showLink=debug, plotlyServerURL="https://chart-studio.plotly.com", toImageButtonOptions = dict(format='svg'))),
                                    html.Br(),

                                    #--------------------------------------------------------------------------
                                    # PLOTTING OPTIONS
                                    #--------------------------------------------------------------------------
                                    html.H5("Konfiguration Plot Feldverteilung"),
                                    html.Hr(),
                                    dbc.Row(
                                        [
                                            #--------------------------------------------------------------------------
                                            # COLORBAR TOGGLE
                                            #--------------------------------------------------------------------------
                                            dbc.Col(
                                                [
                                                    dbc.Label("Ein-/Ausblenden"),
                                                    dbc.Checklist(
                                                        id='plot-colorbar',
                                                        options=[
                                                            {'label': "Colorbar", 'value': True},
                                                        ],
                                                        value=[True],
                                                        switch=True,
                                                    ),
                                                ],
                                                width="auto",
                                                className="mb-3",
                                            ),
                                            #--------------------------------------------------------------------------
                                            # PERPENDICULAR AXIS POSITION
                                            #--------------------------------------------------------------------------
                                            dbc.Col(
                                                [
                                                    dbc.Label("Querschnitt bei"),
                                                    dcc.Slider(
                                                        id='plot-slider',
                                                        min=0,
                                                        value=0,
                                                        step=1,
                                                        updatemode='drag'
                                                    ),
                                                ],
                                                sm=4,
                                                className="mb-3",
                                            ),
                                            #--------------------------------------------------------------------------
                                            # FUNCTION SELECTION
                                            #--------------------------------------------------------------------------
                                            dbc.Col(
                                                [   
                                                    dbc.Label("Darstellung von"),
                                                    dbc.RadioItems(
                                                        id='mathfun-sel',
                                                        options=[{'label': i, 'value': f} for (i,f) in zip(mathfun_labels, mathfun)],
                                                        value=mathfun[0],
                                                        inline=True
                                                    ),
                                                ],
                                                width="auto",
                                                className="mb-3",
                                            ),
                                            #--------------------------------------------------------------------------
                                            # VIEW SELECTION
                                            #--------------------------------------------------------------------------
                                            dbc.Col(
                                                [
                                                    dbc.Label("Betrachtungsebene"),
                                                    dbc.RadioItems(
                                                        id='axes-radio',
                                                        value=views[0],
                                                        inline=True,
                                                        options=[{'label': i, 'value': i, 'disabled': False} for i in views],
                                                    ),
                                                ],
                                                width="auto",
                                                className="mb-3",
                                            ),
                                            #--------------------------------------------------------------------------
                                            # TRACE SELECTION
                                            #--------------------------------------------------------------------------
                                            dbc.Col(
                                                [
                                                    dbc.Label("Plottyp"),
                                                    dbc.RadioItems(
                                                        id='plot-type-radio',
                                                        options=[{'label': i, 'value': i} for i in ("Contour","Heatmap")],#,"3D")],
                                                        value="Contour",
                                                        inline=True
                                                    ),
                                                ],
                                                width="auto",
                                                className="mb-3",
                                            ),
                                        ],
                                    ),
                                ],
                            ),
                            label="Feldverteilung",
                            id='tab-field',
                        ),
                        dbc.Tab(
                            #--------------------------------------------------------------------------
                            # PROPAGATION CONSTANTS PLOT TAB
                            #--------------------------------------------------------------------------
                            dbc.CardBody(
                                [  
                                    #--------------------------------------------------------------------------
                                    # PROPAGATION CONSTANTS PLOT
                                    #--------------------------------------------------------------------------
                                    html.H5("Plot Ausbreitungskonstanten"),
                                    html.Hr(),
                                    dcc.Graph(id='propagation-constant-graph', config=dict(showLink=debug, plotlyServerURL="https://chart-studio.plotly.com", toImageButtonOptions = dict(format='svg'))),
                                    html.Br(),

                                    #--------------------------------------------------------------------------
                                    # PLOTTING OPTIONS (PROPAGATION)
                                    #--------------------------------------------------------------------------
                                    html.H5("Konfiguration Plot Ausbreitungskonstanten"),
                                    html.Hr(),
                                    dbc.Row(
                                        [
                                            dbc.Col(
                                                [
                                                    #--------------------------------------------------------------------------
                                                    # LEGEND TOGGLE
                                                    #--------------------------------------------------------------------------
                                                    dbc.Label("Ein-/Ausblenden"),
                                                    dbc.Checklist(
                                                        id='propagation-axis-legend',
                                                        options=[
                                                            {'label': "Legende", 'value': True},
                                                        ],
                                                        value=[True],
                                                        switch=True,
                                                    ),
                                                ],
                                                width="auto",
                                                className="mb-3",
                                            ),
                                            dbc.Col(
                                                [
                                                    #--------------------------------------------------------------------------
                                                    # AXIS SELECTOR
                                                    #--------------------------------------------------------------------------
                                                    dbc.Label("Größe auf horizontaler Achse"),
                                                    dbc.RadioItems(
                                                        id='propagation-axis-radio',
                                                        options=[{'label': i, 'value': v} for i,v in zip(propagation_axis_label,propagation_axis)],
                                                        value=propagation_axis[0],
                                                        inline=True
                                                    ),
                                                ],
                                                width="auto",
                                                className="mb-3",
                                            ),
                                        ]
                                    )
                                ],
                            ),
                            label="Ausbreitungskonstanten",
                            id='tab-propagation',
                        ),
                        dbc.Tab(
                            #--------------------------------------------------------------------------
                            # WG WAVELENGTH PLOT TAB
                            #--------------------------------------------------------------------------
                            dbc.CardBody(
                                [  
                                    #--------------------------------------------------------------------------
                                    # WG WAVELENGTH PLOT
                                    #--------------------------------------------------------------------------
                                    html.H5("Plot Hohlleiterwellenlängen"),
                                    html.Hr(),
                                    dcc.Graph(id='wavelength-graph', config=dict(showLink=debug, plotlyServerURL="https://chart-studio.plotly.com", toImageButtonOptions = dict(format='svg'))),
                                    html.Br(),

                                    #--------------------------------------------------------------------------
                                    # PLOTTING OPTIONS (PROPAGATION)
                                    #--------------------------------------------------------------------------
                                    html.H5("Konfiguration Plot Hohlleiterwellenlängen"),
                                    html.Hr(),

                                    dbc.Row(
                                        [
                                            dbc.Col(
                                                [
                                                    #--------------------------------------------------------------------------
                                                    # LEGEND TOGGLE
                                                    #--------------------------------------------------------------------------
                                                    dbc.Label("Ein-/Ausblenden"),
                                                    dbc.Checklist(
                                                        id='wavelength-axis-legend',
                                                        options=[
                                                            {'label': "Legende", 'value': True},
                                                        ],
                                                        value=[True],
                                                        switch=True,
                                                    ),
                                                ],
                                                width="auto",
                                                className="mb-3",
                                            ),
                                            dbc.Col(
                                                [
                                                    #--------------------------------------------------------------------------
                                                    # AXIS SELECTOR
                                                    #--------------------------------------------------------------------------
                                                    dbc.Label("Größe auf horizontaler Achse"),
                                                    dbc.RadioItems(
                                                        id='wavelength-axis-radio',
                                                        options=[{'label': i, 'value': v} for i,v in zip(wavelength_axis_label,wavelength_axis)],
                                                        value=wavelength_axis[0],
                                                        inline=True
                                                    ),
                                                ],
                                                width="auto",
                                                className="mb-3",
                                            ),
                                        ]
                                    )
                                ]
                            ),
                            label="Hohlleiterwellenlängen",
                            id='tab-wavelength',
                        ),
                    ],
                    id='card-tabs',
                ),
            ),

            dbc.Col(
                [   
                    dbc.Card(
                        dbc.CardBody(
                            [
                                html.H5("Konfiguration Hohlleiter"),
                                html.Hr(),
                                dbc.Collapse(
                                    [
                                        dbc.Row(
                                            [
                                                dbc.Col(
                                                    [
                                                        #--------------------------------------------------------------------------
                                                        # POLARIZATION & MODE
                                                        #--------------------------------------------------------------------------
                                                        dbc.Label("Hohlleitermode", id="modelabel"),
                                                        dbc.Row(
                                                            [
                                                                dbc.Col(
                                                                    [
                                                                        dbc.RadioItems(
                                                                            id='polarization-radio',
                                                                            options=[{'label': i, 'value': i} for i in polarizations],
                                                                            value=polarizations[0],
                                                                            inline=True,  
                                                                        ),
                                                                        dbc.Popover(
                                                                            dbc.Alert(
                                                                                "Die gewählte Mode ist nicht ausbreitungsfähig!",
                                                                                color="danger", 
                                                                                is_open=True,
                                                                            ),
                                                                            id="mode-popover",
                                                                            is_open=False,
                                                                            target="modelabel",
                                                                            placement="top",
                                                                        ),
                                                                    ],
                                                                    width="auto"
                                                                ),
                                                                dbc.Col(
                                                                    dbc.InputGroup(
                                                                        [
                                                                            dbc.Input(id='m', type='number', value=1, min=0, step=1, style={'maxWidth': '4em'}),
                                                                            dbc.Input(id='n', type='number', value=1, min=0, step=1, style={'maxWidth': '4em'}),
                                                                        ]
                                                                    ),
                                                                    width="auto",
                                                                    className="ml-0",
                                                                )
                                                            ],
                                                            # form=True,
                                                            align="center",
                                                        ),
                                                    ],
                                                    width="auto",
                                                    className="mb-3",
                                                ),
                                                dbc.Col(
                                                    [
                                                        #--------------------------------------------------------------------------
                                                        # FREQUENCY
                                                        #--------------------------------------------------------------------------
                                                        dbc.Label("Frequenz f"),
                                                        dbc.InputGroup(
                                                            [
                                                                dbc.Input(id='f', type='number', value=10, min=0, step=0.1, style={'maxWidth': '6em'}),
                                                                dbc.InputGroupText("GHz"),
                                                            ],
                                                        ),
                                                    ],
                                                    width="auto",
                                                    className="mb-3",
                                                ),
                                                dbc.Col(
                                                    [
                                                        #--------------------------------------------------------------------------
                                                        # WAVEGUIDE GEOMETRY
                                                        #--------------------------------------------------------------------------
                                                        dbc.Label("Breite a"),
                                                        dbc.InputGroup(
                                                            [
                                                                dbc.Input(id='a', type='number', value=50, min=0, step=1, style={'maxWidth': '6em'}),
                                                                dbc.InputGroupText("mm"),
                                                            ],
                                                        ),
                                                    ],
                                                    width="auto",
                                                    className="mb-3",
                                                ),
                                                dbc.Col(
                                                    [
                                                        dbc.Label("Höhe b"),
                                                        dbc.InputGroup(
                                                            [
                                                                dbc.Input(id='b', type='number', value=25, min=0, step=1, style={'maxWidth': '6em'}),
                                                                dbc.InputGroupText("mm"),
                                                            ],
                                                        ),
                                                    ],
                                                    width="auto",
                                                    className="mb-3",
                                                ),
                                            ]
                                        ),
                                        html.Hr(),
                                        dbc.Label("Abgeleitete Größen"),
                                        dbc.Col(
                                            [
                                                dbc.Table(
                                                    html.Tbody(
                                                        [
                                                            # we have to use dcc.Markdown with the dangerously_allow_html flag for subscripts... This is dangerous 
                                                            # html.Tr([html.Td(dcc.Markdown("Aus\xadbreit\xadungs\xadkon\xadstante k<sub>z</sub> \[1/m\]", dangerously_allow_html=True)), html.Td("0", id="kz-table")]),
                                                            # html.Tr([html.Td(dcc.Markdown("Hohl\xadleiter\xadwellen\xadlänge λ<sub>mn</sub> \[m\]", dangerously_allow_html=True)), html.Td("0", id="wl-table")]),
                                                            html.Tr([html.Td("Aus\xadbreit\xadungs\xadkon\xadstante k_z [1/m]"), html.Td("0", id="kz-table")]),
                                                            html.Tr([html.Td("Hohl\xadleiter\xadwellen\xadlänge λ_mn [m]"), html.Td("0", id="wl-table")]),
                                                        ],
                                                    ), 
                                                    bordered=True,
                                                    responsive=True,
                                                )
                                            ],
                                            width="auto",
                                        ),
                                    ],
                                    is_open=False,
                                    id="collapse",
                                ),
                                dbc.Button(
                                    "Konfiguration des Hohlleiters ein-/ausblenden",
                                    id="collapse-open",
                                    color="primary",
                                ),
                            ],
                        ),
                        style={'border': 'none'},
                    )
                ],
            ),
        ],
        fluid=True
    )

def callbacks(app):
    # Check validity callback function
    def check_validity(x):
        if x is None:
            return False, True
        else:
            return False, False
    
    app.callback(
        [Output("a", "valid"), Output("a", "invalid")],
        [Input("a", "value")]
    )(check_validity)

    app.callback(
        [Output("b", "valid"), Output("b", "invalid")],
        [Input("b", "value")]
    )(check_validity)
        
    app.callback(
        [Output("m", "valid"), Output("m", "invalid")],
        [Input("m", "value")]
    )(check_validity)

    app.callback(
        [Output("n", "valid"), Output("n", "invalid")],
        [Input("n", "value")]
    )(check_validity)

    app.callback(
        [Output("f", "valid"), Output("f", "invalid")],
        [Input("f", "value")]
    )(check_validity)

    # Callback for settings
    def toggle_collapse(n, is_open):
        if n:
            return not is_open
        return is_open

    # Callback for collapse close button
    app.callback(
        Output("collapse", "is_open"),
        [Input("collapse-open", "n_clicks")],
        [State("collapse", "is_open")],
    )(toggle_collapse)


    @app.callback(
        Output("fcomp-dropdown", "label"),
        [Input(i+"_dropitem", "n_clicks") for i in fcomps],
    )
    def update_dropdown_label(*arg):
        # use a dictionary to map ids back to the desired label
        # makes more sense when there are lots of possible labels
        id_lookup = {i+"_dropitem":i for i in fcomps}

        ctx = dash.callback_context

        if not any(arg) or not ctx.triggered:
            # if neither button has been clicked, return default
            return fcomps[0]

        # this gets the id of the button that triggered the callback
        button_id = ctx.triggered[0]["prop_id"].split(".")[0]
        return id_lookup[button_id]

    # Callbacks for slider
    @app.callback(
        [Output('plot-slider', 'max'),
        Output('plot-slider', 'marks')],
        [Input('axes-radio', 'value'),]
    )
    def slider_labels(plane):

        if plane == "xy":
            max = Mz-1
            marks = {k: {'label': '' if mod(k,Mz//4) else 'z/L=' + str(around(x,2)), 'style': {'fontSize': '1.25em'}} for k,x in enumerate(linspace(0, 1, num=Mz, endpoint=True))}

        elif plane == "xz":
            max = My-1
            marks = {k: {'label': '' if mod(k,My//4) else 'y/b=' + str(around(x,2)), 'style': {'fontSize': '1.25em'}} for k,x in enumerate(linspace(0, 1, num=My, endpoint=True))}

        elif plane == "yz":
            max = Mx-1
            marks = {k: {'label': '' if mod(k,Mx//4) else 'x/a=' + str(around(x,2)), 'style': {'fontSize': '1.25em'}} for k,x in enumerate(linspace(0, 1, num=Mx, endpoint=True))}

        else:
            max = Mz-1
            marks = {k: {'label': '' if mod(k,Mz//4) else 'z/L=' + str(around(x,2)), 'style': {'fontSize': '1.25em'}} for k,x in enumerate(linspace(0, 1, num=Mz, endpoint=True))}

        return max,marks


    # Disable slider for 3D plot
    @app.callback(
        Output('plot-slider','disabled'),
        Input('axes-radio', 'value'),
    )
    def disables_for_3D(x):
        if x == "3D":
            return True
        else:
            return False


    # Field plot
    @app.callback(
        [Output('field-graph', 'figure'),
        Output('mode-popover', 'is_open'),
        Output('kz-table', 'children'),
        Output('wl-table', 'children')],
        [Input('plot-slider', 'value'),
        Input('polarization-radio', 'value'),
        Input('m', 'value'),
        Input('n', 'value'),
        Input('a', 'value'),
        Input('b', 'value'),
        Input('f', 'value'),
        Input('axes-radio', 'value'),
        Input('fcomp-dropdown', 'label'),
        Input('plot-type-radio', 'value'),
        Input('mathfun-sel', 'value'),
        Input('plot-colorbar', 'value'),],
    )
    def update_field_plot(k, pol, m, n, a, b, f, plane, fcomp, plttype, mathfun, cb):

        if (m is None) or (n is None) or (a is None) or (b is None) or (f is None):
            raise dash.exceptions.PreventUpdate()


        wg = wgsetup(Mx,My,Mz,m,n,a,b,f,pol)

        if plane == "3D":
            M = generate_field(wg, pol, fcomp, wg.tp, "", k, getattr(numpy, mathfun))
        else:
            M = generate_field(wg, pol, fcomp, wg.tp, plane, k, getattr(numpy, mathfun))

        # parameters depending on observation plane
        if plane == "xy":
            x = wg.x
            y = wg.y

            xtv = [0,wg.a]
            ytv = [0,wg.b]
            xtt = ['0','a']
            ytt = ['0','b']

            xlbl = 'x'
            ylbl = 'y'

            wgx = wg.xywg['x']
            wgy = wg.xywg['y']

        elif plane == "xz":
            x = wg.x
            y = wg.z

            xtv = [0,wg.a]
            ytv = [0,wg.L]
            xtt = ['0','a']
            ytt = ['0','L']
            
            xlbl = 'x'
            ylbl = 'z'

            wgx = wg.xzwg['x']
            wgy = wg.xzwg['y']

        elif plane == "yz":
            x = wg.y
            y = wg.z

            xtv = [0,wg.b]
            ytv = [0,wg.L]
            xtt = ['0','b']
            ytt = ['0','L']
            
            xlbl = 'y'
            ylbl = 'z'

            wgx = wg.yzwg['x']
            wgy = wg.yzwg['y']
            
       
        F = squeeze(M.F)
        Fmin = 0
        Fmax = 0

        if mathfun == "abs":
            Fmin = 0
            Fmax = M.Flim
            cscale = "reds"
            vallbl = '|' + fcomp + '|'
        elif mathfun == "angle":
            Fmin = -pi
            Fmax = pi
            cscale = "RdBu_r"
            vallbl = 'arg{' + fcomp + '}'
        else:
            Fmin = -M.Flim
            Fmax = M.Flim
            cscale = "RdBu_r"

            if mathfun == "real":
                vallbl = 'Re{' + fcomp + '}'
            else:
                vallbl = 'Im{' + fcomp + '}'

        if Fmax == 0:
            Fmin = -10*finfo(float32).resolution
            Fmax = -Fmin


        show_cb = True
        if not cb:
            show_cb = False
        
        #--------------------------------------------------------------------------
        # 3D
        #--------------------------------------------------------------------------
        # if plttype == "3D":
        if plane == "3D":
            every_nth = 2

            xg,yg,zg = meshgrid(wg.z[::every_nth],wg.y[::every_nth],wg.x[::every_nth],indexing='ij')

            wgx = wg.wg3d['z']
            wgy = wg.wg3d['y']
            wgz = wg.wg3d['x']

            xlbl = 'x'
            ylbl = 'y'
            zlbl = 'z'

            if plttype == "Contour":
                fig = go.Figure(
                    go.Isosurface(
                        x=xg.flatten(),
                        y=yg.flatten(),
                        z=zg.flatten(),
                        value=F[::every_nth,::every_nth,::every_nth].transpose((2,1,0)).flatten(),
                        name=fcomp,
                        isomin=None,
                        isomax=None,
                        surface_count=8, # number of isosurfaces, 2 by default: only min and max
                        colorscale=cscale,
                        showscale=show_cb,
                        caps=dict(x_show=False, y_show=False, z_show=False),
                        hovertemplate=xlbl+': %{z}'+'<br>'+ylbl+': %{y}<br>'+zlbl+': %{x}<br>'+vallbl+': %{value}<extra></extra>'
                    ), layout=layout3d
                )
            else:
                fig = go.Figure(
                    go.Volume(
                        x=xg.flatten(),
                        y=yg.flatten(),
                        z=zg.flatten(),
                        value=F[::every_nth,::every_nth,::every_nth].transpose((2,1,0)).flatten(),
                        name=fcomp, 
                        isomin=None,
                        isomax=None,
                        opacity=0.2, # needs to be small to see through all surfaces
                        surface_show=True,
                        surface_count=5, # needs to be a large number for good volume rendering
                        colorscale=cscale,
                        showscale=show_cb,
                        opacityscale='extremes',
                        slices_x_show=True,
                        slices_y_show=True,
                        slices_z_show=True,
                        hovertemplate=xlbl+': %{z}'+'<br>'+ylbl+': %{y}<br>'+zlbl+': %{x}<br>'+vallbl+': %{value}<extra></extra>'
                    ), layout=layout3d
                )
            
            fig.add_trace(go.Scatter3d(x=wgx, y=wgy, z=wgz, showlegend=False, line_color="black", line_width=3, mode="lines", hoverinfo="skip"))
            fig.update_layout(
                scene_xaxis_tickvals=[0,wg.L], scene_xaxis_ticktext=['0','L'], scene_xaxis_range=[wg.L,0],
                scene_yaxis_tickvals=[0,wg.b], scene_yaxis_ticktext=['0','b'], scene_yaxis_range=[0,wg.b],
                scene_zaxis_tickvals=[0,wg.a], scene_zaxis_ticktext=['0','a'], scene_zaxis_range=[0,wg.a],
            )

        #--------------------------------------------------------------------------
        # 2D
        #--------------------------------------------------------------------------
        else:
            if plttype == "Contour":
                fig = go.Figure(go.Contour(x=x, y=y, z=F, name=fcomp, zmin=Fmin, zmax=Fmax, colorscale=cscale, showscale=show_cb, transpose=True, hovertemplate=xlbl+': %{x}'+'<br>'+ylbl+': %{y}<br>'+vallbl+': %{z}<extra></extra>'), layout=layout2d)
            elif plttype == "Heatmap":
                fig = go.Figure(go.Heatmap(x=x, y=y, z=F, name=fcomp, zmin=Fmin, zmax=Fmax, colorscale=cscale, showscale=show_cb, transpose=True, zsmooth = 'best', hovertemplate=xlbl+': %{x}'+'<br>'+ylbl+': %{y}<br>'+vallbl+': %{z}<extra></extra>'), layout=layout2d)

            fig.add_trace(go.Scatter(x=wgx, y=wgy, showlegend=False, line_color="black", line_width=2, mode="lines", hoverinfo="skip"))
            
            fig.update_layout(
                xaxis_tickvals=xtv, xaxis_ticktext=xtt, xaxis_title=xlbl, 
                yaxis_tickvals=ytv, yaxis_ticktext=ytt, yaxis_title=ylbl, 
            )

        # complex values
        kz_str = format_float_positional(sqrt(wg.kz2),precision=4,unique=True,fractional=False)
        wlwg_str = format_float_positional(2*pi/sqrt(wg.kz2),precision=4,unique=True,fractional=False)

        if wg.propagates:
            return fig, False, kz_str, wlwg_str
        else:
            return fig, True, kz_str, wlwg_str



    # Propagation constant plot
    @app.callback(
        Output('propagation-constant-graph', 'figure'),
        [Input('polarization-radio', 'value'),
        Input('m', 'value'),
        Input('n', 'value'),
        Input('a', 'value'),
        Input('b', 'value'),
        Input('f', 'value'),
        Input('propagation-axis-radio', 'value'),
        Input('propagation-axis-legend', 'value'),
        ],
    )
    def update_propagation_plot(pol,m,n,a,b,f,param,lgd):
        if (m is None) or (n is None) or (a is None) or (b is None) or (f is None):
            raise dash.exceptions.PreventUpdate()

        wg = wgsetup(Mx,My,Mz,m,n,a,b,f,pol)
        modes = [(m-1,m), (m,n-1), (m,n), (m,n+1), (m+1,n), (m+1,n+1)]
        
        if pol == "H":
            modes = list(filter(lambda x: x[0] + x[1] > 0, modes))
        else:
            modes = list(filter(lambda x: x[0] + x[1] > 1, modes))


        if param == "f":
            ω = 2*pi*linspace(0, 2*wg.f, num=10000)
            kz_mn = lambda m,n: sqrt((ω**2/wg.c**2 - (m*pi/wg.a)**2 - (n*pi/wg.b)**2).astype(complex))
        
            xlbl = "Frequenz [GHz]"
            xr = [0, 2*wg.f/1e9]
            line = dict(
                type = 'line',
                xref = 'x', x0 = wg.f*1e-9, x1 = wg.f*1e-9,
                yref = 'paper', y0 = 0, y1 = 1,
                line_color = '#444444'
            )
            text = dict(
                text = "Betriebsfrequenz f",
                textangle = -90,
                xref = 'x', x = wg.f*1e-9,
                yref = 'paper', y = 0.5, yanchor = 'middle',
                bgcolor = '#ffffff',
                showarrow = False,
            )

            x = ω/(2*pi*1e9)

        elif param == "a":
            a = linspace(1e-8, 2*wg.a, num=10000)
            kz_mn = lambda m,n: sqrt(((2*pi*wg.f)**2/wg.c**2 - (m*pi/a)**2 - (n*pi/wg.b)**2).astype(complex))

            xlbl = "Breite [mm]"
            xr = [0, 2*wg.a*1e3]
            line = dict(
                type = 'line',
                xref = 'x', x0 = wg.a*1e3, x1 = wg.a*1e3,
                yref = 'paper', y0 = 0, y1 = 1,
                line_color = '#444444'
            )
            text = dict(
                text = "Gewählte Breite a",
                textangle = -90,
                xref = 'x', x = wg.a*1e3,
                yref = 'paper', y = 0.5, yanchor = 'middle',
                bgcolor = '#ffffff',
                showarrow = False,
            )

            x = a*1e3
        
        elif param == "b":
            b = linspace(1e-8, 2*wg.b, num=10000)
            kz_mn = lambda m,n: sqrt(((2*pi*wg.f)**2/wg.c**2 - (m*pi/wg.a)**2 - (n*pi/b)**2).astype(complex))

            xlbl = "Höhe [mm]"
            xr = [0, 2*wg.b*1e3]
            line = dict(
                type = 'line',
                xref = 'x', x0 = wg.b*1e3, x1 = wg.b*1e3,
                yref = 'paper', y0 = 0, y1 = 1,
                line_color = '#444444'
            )
            text = dict(
                text = "Gewählte Höhe b",
                textangle = -90,
                xref = 'x', x = wg.b*1e3,
                yref = 'paper', y = 0.5, yanchor = 'middle',
                bgcolor = '#ffffff',
                showarrow = False,
            )

            x = b*1e3

        
        ylbl = "Ausbreitungskonstante k<sub>z</sub> [m<sup>-1</sup>]"

        y = [kz_mn(*m) for m in modes]
        mask = [~isreal(ym) for ym in y]

        x = [ma.array(x, mask=m).filled(nan) for m in mask]
        y = [ma.array(ym.real, mask=m).filled(nan) for (ym,m) in zip(y,mask)]

        fig = go.Figure(
            data=[go.Scatter(x=x[k], y=y[k], showlegend=True, name=pol + "<sub>" + str(m[0]) + str(m[1]) + "</sub>") for k,m in enumerate(modes)],
            layout=layout1d
        )

        fig.update_layout(
            xaxis_range = xr,
            xaxis_title = xlbl,
            yaxis_title = ylbl,
            shapes = [line],
            annotations = [text]
        )

        if not lgd:
            fig.update_layout(
                showlegend = False
            )

        return fig


    
    # waveguide wavelength plot
    @app.callback(
        Output('wavelength-graph', 'figure'),
        [Input('polarization-radio', 'value'),
        Input('m', 'value'),
        Input('n', 'value'),
        Input('a', 'value'),
        Input('b', 'value'),
        Input('f', 'value'),
        Input('wavelength-axis-radio', 'value'),
        Input('wavelength-axis-legend', 'value'),
        ],
    )
    def update_wavelength_plot(pol,m,n,a,b,f,param,lgd):
        if (m is None) or (n is None) or (a is None) or (b is None) or (f is None):
            raise dash.exceptions.PreventUpdate()

        wg = wgsetup(Mx,My,Mz,m,n,a,b,f,pol)
        modes = [(m-1,m), (m,n-1), (m,n), (m,n+1), (m+1,n), (m+1,n+1)]
        
        if pol == "H":
            modes = list(filter(lambda x: x[0] + x[1] > 0, modes))
        else:
            modes = list(filter(lambda x: x[0] + x[1] > 1, modes))

        if param == "f":
            ω = 2*pi*linspace(wg.f/100, 2*wg.f, num=10000)
            lambda_mn = lambda m,n: 2*pi/sqrt((ω**2/wg.c**2 - (m*pi/wg.a)**2 - (n*pi/wg.b)**2).astype(complex))
        
            xlbl = "Frequenz [GHz]"
            xr = [0, 2*wg.f/1e9]
            line = dict(
                type = 'line',
                xref = 'x', x0 = wg.f*1e-9, x1 = wg.f*1e-9,
                yref = 'paper', y0 = 0, y1 = 1,
                line_color = '#444444'
            )
            text = dict(
                text = "Betriebsfrequenz f",
                textangle = -90,
                xref = 'x', x = wg.f*1e-9,
                yref = 'paper', y = 0.5, yanchor = 'middle',
                bgcolor = '#ffffff',
                showarrow = False,
            )

            x0 = ω/(2*pi*1e9)
            yref = 2*pi*wg.c/ω

        elif param == "a":
            a = linspace(1e-8, 2*wg.a, num=10000)
            lambda_mn = lambda m,n: 2*pi/sqrt(((2*pi*wg.f)**2/wg.c**2 - (m*pi/a)**2 - (n*pi/wg.b)**2).astype(complex))

            xlbl = "Breite [mm]"
            xr = [0, 2*wg.a*1e3]
            line = dict(
                type = 'line',
                xref = 'x', x0 = wg.a*1e3, x1 = wg.a*1e3,
                yref = 'paper', y0 = 0, y1 = 1,
                line_color = '#444444'
            )
            text = dict(
                text = "Gewählte Breite a",
                textangle = -90,
                xref = 'x', x = wg.a*1e3,
                yref = 'paper', y = 0.5, yanchor = 'middle',
                bgcolor = '#ffffff',
                showarrow = False,
            )

            x0 = a*1e3
            yref = wg.c/wg.f*ones(x0.shape)
        
        elif param == "b":
            b = linspace(1e-8, 2*wg.b, num=10000)
            lambda_mn = lambda m,n: 2*pi/sqrt(((2*pi*wg.f)**2/wg.c**2 - (m*pi/wg.a)**2 - (n*pi/b)**2).astype(complex))

            xlbl = "Höhe [mm]"
            xr = [0, 2*wg.b*1e3]
            line = dict(
                type = 'line',
                xref = 'x', x0 = wg.b*1e3, x1 = wg.b*1e3,
                yref = 'paper', y0 = 0, y1 = 1,
                line_color = '#444444'
            )
            text = dict(
                text = "Gewählte Höhe a",
                textangle = -90,
                xref = 'x', x = wg.b*1e3,
                yref = 'paper', y = 0.5, yanchor = 'middle',
                bgcolor = '#ffffff',
                showarrow = False,
            )

            x0 = b*1e3
            yref = wg.c/wg.f*ones(x0.shape)

        
        ylbl = "Hohlleiterwellenlänge λ<sub>mn</sub> [m]"

        y = [lambda_mn(*m) for m in modes]
        mask = [~isreal(ym) for ym in y]

        x = [ma.array(x0, mask=m).filled(nan) for m in mask]
        y = [ma.array(ym.real, mask=m).filled(nan) for (ym,m) in zip(y,mask)]

        fig = go.Figure(
            data=[go.Scatter(x=x[k], y=y[k], showlegend=True, name=pol + "<sub>" + str(m[0]) + str(m[1]) + "</sub>") for k,m in enumerate(modes)],
            layout=layout1d
        )
        fig.add_trace(go.Scatter(x=x0, y=yref, showlegend=True, line_color="black", line_dash="dash", name="λ<sub>0</sub>"))

        fig.update_layout(
            xaxis_range = xr,
            xaxis_title = xlbl,
            yaxis_range = [0,wg.c/wg.f*10],
            yaxis_title = ylbl,
            shapes = [line],
            annotations = [text]
        )

        if not lgd:
            fig.update_layout(
                showlegend = False
            )

        return fig


    return app
