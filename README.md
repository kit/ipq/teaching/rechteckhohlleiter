# Rechteckhohlleiter

Note: If you do not want to host the dash app under the alternate path .../rechteckhohlleiter, remove the parameter "requests_pathname_prefix" in src/dashapp.py. This procedure will be necessary to run the app without an additional http server such as apache.

# Setup
Clone the repository
```
git clone https://git.scc.kit.edu/mv6955/rechteckhohlleiter
```
Change in into the directory and create a virtual python environment
```
cd rechteckhohlleiter
python -m venv env
```
Activate the environment using ```.\env\Scripts\activate``` on Windows or ```source env/bin/activate``` on Linux (bash)
To install all the dependencies, run 
```
pip install .
```
Start a local instance of the dash application with
```
cd src
python dashapp.py
```
